using GuestlineTechnicalChallenge;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GuestlineTechnicalChallenge.Classes;
using System;
using System.Collections.Generic;

namespace GuestlineTests
{
    [TestClass]
    public class UnitTests
    {
        ////INITIALISATION
        //public void GridInitialisation()
        //public void ShipPlacement()


        ////GAMEPLAY
        //public void ProcessMissedShot()
        //public void ProcessHitShot()
        //public void ProcessFullySinkedShip()
        //public void GameEndsWhenGameComplete()

        private Gameboard ArrangeGameboard()
        {
            return new Gameboard()
            {
                grid = new Tile[10, 10],
                ships = new List<Ship>() { new Ship() { shipLength = 5, shipTiles = new Tile[5], shipType = "Destroyer" } },
                sinks = 0,
                hits = 0,
                misses = 0,
                turns = 0,
                gameComplete = false
            };
        }

        //VALIDATION 
        [TestMethod]
        public void HandlesIncorrectFormatDoubleLetter()
        {
            Gameboard mockGameboard = ArrangeGameboard();
            mockGameboard.grid[0, 0] = new Tile(0, 0) { tileStatus = TileStatus.Miss };
            string input = "AA";

            bool actual = Program.ValidateInput(input, mockGameboard.grid);

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void HandlesIncorrectFormatDoubleNumber()
        {
            Gameboard mockGameboard = ArrangeGameboard();
            mockGameboard.grid[0, 0] = new Tile(0, 0) { tileStatus = TileStatus.Miss };
            string input = "00";

            bool actual = Program.ValidateInput(input, mockGameboard.grid);

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void HandlesIncorrectFormatIncorrect()
        {
            Gameboard mockGameboard = ArrangeGameboard();
            mockGameboard.grid[0, 0] = new Tile(0, 0) { tileStatus = TileStatus.Miss };
            string input = "AA0";

            bool actual = Program.ValidateInput(input, mockGameboard.grid);

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void HandlesInvalidInputOutOfRange()
        {
            Gameboard mockGameboard = ArrangeGameboard();
            mockGameboard.grid[0, 0] = new Tile(0, 0) { tileStatus = TileStatus.Miss };
            string input = "P0";

            bool actual = Program.ValidateInput(input, mockGameboard.grid);

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void HandlesInvalidInputAlreadyFired()
        {
            Gameboard mockGameboard = ArrangeGameboard();
            mockGameboard.grid[0, 0] = new Tile(0, 0) { tileStatus = TileStatus.Miss };
            string input = "A0";

            bool actual = Program.ValidateInput(input, mockGameboard.grid);

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void HandlesValidInput()
        {
            Gameboard mockGameboard = ArrangeGameboard();
            mockGameboard.grid[0, 0] = new Tile(0, 0) { tileStatus = TileStatus.Hidden };
            string input = "A0";

            bool actual = Program.ValidateInput(input, mockGameboard.grid);

            Assert.IsTrue(actual);
        }
    }
}
