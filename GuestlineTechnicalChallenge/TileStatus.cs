﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuestlineTechnicalChallenge
{
    public enum TileStatus
    {
        Hidden,
        Hit,
        Miss,
        Sunk
    }
}
