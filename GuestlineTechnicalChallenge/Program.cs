﻿using GuestlineTechnicalChallenge.Classes;
using GuestlineTechnicalChallenge.Properties;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GuestlineTechnicalChallenge
{
    public class Program
    {
        static void Main(string[] args)
        {
            LoadGameMenu();
            Console.WriteLine("Welcome to Guestline Battleships");
        }

        public static void LoadGameMenu()
        {
            Console.WriteLine("Welcome to Guestline Battleships");
            Console.WriteLine("Press The Enter Key to Start");

            Console.ReadLine();

            Gameboard gameBoard = InitialiseGame();

            PlayGame(gameBoard);

            Console.WriteLine("\nThanks for playing Battleships");
            Console.ReadLine();
        }

        public static void PlayGame(Gameboard gameboard)
        {
            do
            {
                DisplayTurn(gameboard);
                 
                ProcessShot(gameboard);

                gameboard = CheckGameStatus(gameboard);
            } while (gameboard.gameComplete == false);
        }

        public static Gameboard CheckGameStatus(Gameboard gameboard)
        {
            if (gameboard.sinks == gameboard.ships.Count)
            {
                DisplayTurn(gameboard);
                Console.WriteLine("All Battleships Sunk! Well done!");
                Console.WriteLine(string.Concat("\n||| You completed your game in ", gameboard.turns, " Turns with ", gameboard.hits, " Hits and ", gameboard.misses, " Misses |||"));
                gameboard.gameComplete = true;
            }              
            return gameboard;
        }

        public static Gameboard InitialiseGame()
        {
            return InitialiseGameboard();
        }

        private static Gameboard InitialiseGameboard()
        {
            Gameboard gameboard = new Gameboard();

            gameboard.grid = InitialiseGrid(10, 10);
            gameboard.ships = PlaceBattleships(gameboard, 1, 2);

            return gameboard;
        }

        private static Tile[,] InitialiseGrid(int gridWidth, int gridHeight)
        {
            Tile[,] grid = new Tile[gridWidth, gridHeight];

            for (int i = 0; i < gridWidth; i++)
            {
                for (int j = 0; j < gridHeight; j++)
                {
                    grid[i, j] = new Tile(i, j);
                }
            }
            return grid;
        }

        public static void ProcessShot(Gameboard gameboard)
        {
            Console.WriteLine("\nTake your next turn by entering a co-ordinate in the format: A0");
            string input = Console.ReadLine();
            Console.WriteLine(input);

            if (ValidateInput(input, gameboard.grid))
            {
                int[] convertedInput = ConvertToTrueCoordinates(input);
                if (CheckHit(convertedInput[0], convertedInput[1], gameboard.ships.SelectMany(s => s.shipTiles).ToList()))
                {
                    gameboard.grid[convertedInput[0], convertedInput[1]].tileStatus = TileStatus.Hit;
                    gameboard.hits++;
                    CheckForSinkAndUpdate(gameboard);
                }
                else
                {
                    gameboard.grid[convertedInput[0], convertedInput[1]].tileStatus = TileStatus.Miss;
                    gameboard.misses++;
                }
                gameboard.turns++;
            }
            else
            {
                Console.WriteLine("Invalid Input.");
            }
        }

        private static void CheckForSinkAndUpdate(Gameboard gameboard)
        {
            foreach (Ship ship in gameboard.ships)
            {
                int tileHits = 0;
                foreach (Tile tile in ship.shipTiles)
                {
                    if (tile.tileStatus == TileStatus.Sunk)
                        break;

                    if (gameboard.grid[tile.xCoords, tile.yCoords].tileStatus == TileStatus.Hit)
                    {
                        //Update the ship tileList
                        tile.tileStatus = TileStatus.Hit;
                        tileHits++;
                    }
                }

                if (tileHits == ship.shipLength)
                {
                    //Update all tiles to 'Sunk' and incrememnt the sink counter
                    foreach (Tile tile in ship.shipTiles)
                    {
                        gameboard.grid[tile.xCoords, tile.yCoords].tileStatus = TileStatus.Sunk;
                        tile.tileStatus = TileStatus.Sunk;
                    }
                    gameboard.sinks++;
                }
            }
        }

        private static int[] ConvertToTrueCoordinates(string input)
        {
            char letterInput = input.Substring(0, 1).ToCharArray()[0];
            int xInput = Convert.ToInt32(input.Substring(1, 1));
            int yInput = char.ToUpper(letterInput) - 65;

            return new int[2] { xInput, yInput };
        }

        private static bool CheckHit(int x, int y, List<Tile> tiles)
        {
            foreach (Tile tile in tiles)
            {
                if (tile.xCoords == x && tile.yCoords == y)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool ValidateInput(string input, Tile[,] grid)
        {
            if (input.Length != 2 || !Char.IsLetter(input, 0) || !Char.IsNumber(input, 1))
            {
                Console.WriteLine(Resources.InvalidFormatError);
                return false;
            }

            int[] trueInput = ConvertToTrueCoordinates(input);
            if (trueInput[0] > grid.GetLength(0) || trueInput[1] > grid.GetLength(1)) {
                Console.WriteLine(Resources.OutOfRangeError);
                return false;
            }

            if (grid[trueInput[0], trueInput[1]].tileStatus == TileStatus.Hit)
            {
                Console.WriteLine(Resources.AlreadyHitError);
                return false;
            }

            if (grid[trueInput[0], trueInput[1]].tileStatus == TileStatus.Miss)
            {
                Console.WriteLine(Resources.AlreadyMissedError);
                return false;
            }

            if (grid[trueInput[0], trueInput[1]].tileStatus == TileStatus.Sunk)
            {
                Console.WriteLine(Resources.AlreadySunkError);
                return false;
            }
            return true;
        }

        private static void DisplayTurn(Gameboard gameboard)
        {
            DrawGrid(gameboard);

            DrawTurnInfo(gameboard);
        }

        private static void DrawTurnInfo(Gameboard gameboard)
        {
            Console.WriteLine("\n--------------------------------------");

            Console.WriteLine(string.Concat("Turns: ", gameboard.turns, " | Hits: ", gameboard.hits, " | Misses: ", gameboard.misses, " | Sinks: ", gameboard.sinks));

            Console.WriteLine("--------------------------------------");
        }

        private static void DrawGrid(Gameboard gameboard)
        {
            for (int i = 0; i < gameboard.grid.GetLength(0); i++)
            {
                Console.WriteLine();

                DrawAxis(gameboard.grid.GetLength(1), i);

                for (int j = 0; j < gameboard.grid.GetLength(1); j++)
                {
                    switch (gameboard.grid[i, j].tileStatus)
                    {
                        case TileStatus.Hidden:
                            Console.Write(" ~ ");
                            break;
                        case TileStatus.Hit:
                            Console.Write(" X ");
                            break;
                        case TileStatus.Miss:
                            Console.Write(" O ");
                            break;
                        case TileStatus.Sunk:
                            Console.Write(" S ");
                            break;
                    }
                }
            }
        }

        private static void DrawAxis(int width, int i)
        {
            if (i == 0)
            {
                Console.Write(" ");
                for (int j = 0; j < width; j++)
                {
                    Console.Write(" " + Convert.ToChar(j + 65) + " ");  //<-- Convert to Letter
                }
                Console.WriteLine();
            }
            Console.Write(i);
        }

        private static List<Ship> PlaceBattleships(Gameboard gameboard, int battleships, int destroyers)
        {
            List<Ship> gameboardShips = new List<Ship>();
            Random rnd = new Random();
            int gridHeight = gameboard.grid.GetLength(0);
            int gridWidth = gameboard.grid.GetLength(1);

            //BATTLESHIP
            for (int i = 0; i < battleships; i++)
            {
                Ship battleship = new Ship() { shipLength = 5, shipType = "Battleship", shipTiles = new Tile[5] };
                if (rnd.Next(2) == 0)
                {
                    //Vertical Orientation
                    bool validPosition;
                    do
                    {
                        int x = rnd.Next(gridHeight);
                        int y = rnd.Next(gridWidth);
                        validPosition = validateShipPlacement(gridHeight, gameboardShips.SelectMany(s => s.shipTiles).ToList(), battleship, x, y, "Vertical");
                        if (validPosition)
                            battleship.shipTiles = setShipTiles(x, y, "Vertical", battleship.shipLength);
                    } while (validPosition == false);
                }
                else
                {
                    //Horizontal Orientation
                    bool validPosition;
                    do
                    {
                        int x = rnd.Next(gridHeight);
                        int y = rnd.Next(gridWidth);
                        validPosition = validateShipPlacement(gridWidth, gameboardShips.SelectMany(s => s.shipTiles).ToList(), battleship, x, y, "Horizontal");
                        if (validPosition)
                            battleship.shipTiles = setShipTiles(x, y, "Horizontal", battleship.shipLength);
                    } while (validPosition == false);
                }
                gameboardShips.Add(battleship);
            }

            //DESTROYER
            for (int i = 0; i < destroyers; i++)
            {
                Ship destroyer = new Ship() { shipLength = 4, shipType = "Destroyer", shipTiles = new Tile[4] };
                if (rnd.Next(2) == 0)
                {
                    //Vertical Orientation
                    bool validPosition;
                    do
                    {
                        int x = rnd.Next(gridHeight);
                        int y = rnd.Next(gridWidth);
                        validPosition = validateShipPlacement(gridHeight, gameboardShips.SelectMany(s => s.shipTiles).ToList(), destroyer, x, y, "Vertical");
                        if (validPosition)
                            destroyer.shipTiles = setShipTiles(x, y, "Vertical", destroyer.shipLength);
                    } while (validPosition == false);
                }
                else
                {
                    //Horizontal Orientation
                    bool validPosition;
                    do
                    {
                        int x = rnd.Next(gridHeight);
                        int y = rnd.Next(gridWidth);
                        validPosition = validateShipPlacement(gridWidth, gameboardShips.SelectMany(s => s.shipTiles).ToList(), destroyer, x, y, "Horizontal");
                        if (validPosition)
                            destroyer.shipTiles = setShipTiles(x, y, "Horizontal", destroyer.shipLength);
                    } while (validPosition == false);
                }
                gameboardShips.Add(destroyer);
            }

            return gameboardShips;
        }

        private static Tile[] setShipTiles(int x, int y, string orientation, int shipLength)
        {
            List<Tile> shipTiles = new List<Tile>();
            if (orientation == "Vertical")
            {
                for (int i = 0; i < shipLength; i++)
                {
                    shipTiles.Add(new Tile(x, y + i));
                }
            }
            else
            {
                for (int i = 0; i < shipLength; i++)
                {
                    shipTiles.Add(new Tile(x + i, y));
                }
            }
            return shipTiles.ToArray();
        }

        private static bool validateShipPlacement(int gridLength, List<Tile> shipTiles, Ship battleship, int x, int y, string orientation)
        {
            if (orientation == "Vertical")
            {
                //Off board?
                if (y + battleship.shipLength > gridLength)
                {
                    return false;
                }

                //Ship clash?
                foreach (Tile currentShipTile in shipTiles)
                {
                    for (int i = 0; i < battleship.shipLength; i++)
                    {
                        if (currentShipTile.xCoords == x && currentShipTile.yCoords == y + i)
                        {
                            return false;
                        }
                    }
                }
            }
            else
            {
                //Off board?
                if (x + battleship.shipLength > gridLength)
                {
                    return false;
                }

                //Ship clash?
                foreach (Tile currentShipTile in shipTiles)
                {
                    for (int i = 0; i < battleship.shipLength; i++)
                    {
                        if (currentShipTile.xCoords == x + i && currentShipTile.yCoords == y)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    }
}
