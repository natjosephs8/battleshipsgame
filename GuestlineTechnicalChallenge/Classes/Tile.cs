﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuestlineTechnicalChallenge.Classes
{
    public class Tile
    {
        public Tile(int x, int y)
        {
            xCoords = x;
            yCoords = y;
            tileStatus = TileStatus.Hidden;
        }
        //X Co-ordinates
        public int xCoords { get; set; }
        //Y Co-ordinates
        public int yCoords { get; set; }

        //Tile Status (Hidden, Hit, Miss, Sunk)
        public TileStatus tileStatus { get; set; }
    }
}
