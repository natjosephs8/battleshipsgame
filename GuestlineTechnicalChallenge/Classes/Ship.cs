﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuestlineTechnicalChallenge.Classes
{
    public class Ship
    {
        //Ship Type
        public string shipType { get; set; }
        //Ship Length
        public int shipLength { get; set; }
        //Ship Tiles
        public Tile[] shipTiles { get; set; }
    }
}
