﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuestlineTechnicalChallenge.Classes
{
    public class Gameboard
    {
        //Game Grid
        public Tile[,] grid { get; set; }
        //List of Ships
        public List<Ship> ships { get; set; }

        //Turn Counter
        public int turns { get; set; }
        //Hit Counter
        public int hits { get; set; }
        //Miss Counter
        public int misses { get; set; }
        //Sunk Counter
        public int sinks { get; set; }

        //Game Complete
        public bool gameComplete { get; set; }
    }
}
