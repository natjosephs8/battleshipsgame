This is my (Nathaniel Josephs) completed submission for the Guestline Technical Task. 

A Battleships game created in a standard C# Console Application (.NET Core 3.1).
Simply run by opening the GuestlineTechnicalChallenge.sln in Visual Studio 2019 (or newer) 
and debugging the GuestlineTechnicalChallenge.csproj (ensuring that it is your startup project).

Follow Instructions on screen,
Press Any Key to Start,
Enter your input in the format LetterNumber (e.g. A5)
Close the Console Window when finished

Unit Tests have been completed for Validation. However, there was the intent to get complete coverage
(As I had begun to outline) yet time constraints during this week in my none working hours wouldn't allow it.

Thanks for the opportunity to develop this, its one of the more entertaining technical tasks I've had to do :)

Would love any feedback on what standards you were expecting/I may have(n't) hit.

-Nathaniel Josephs

P.S. I'm aware there may be a bug with the x, y coordinates being named incorrectly in some places. However, functionality is correct. Just don't have time to go through individually and refactor where these are the wrong way round (naming)
